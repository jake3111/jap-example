package jap.example.Repository;

import jap.example.Domain.Lesson;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface LessonRepository extends CrudRepository<Lesson, Long> {

    @Query(value="select * from lessons where title = ?1", nativeQuery = true)
    List<Lesson> findByLessonTitle(String title);

    @Query(value="select * from lessons where delivered_on BETWEEN ?1 AND ?2", nativeQuery = true)
    List<Lesson> findByDeliveredOnDate(String date1, String date2);
}
