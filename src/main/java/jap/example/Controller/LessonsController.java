package jap.example.Controller;

import jap.example.Domain.Lesson;
import jap.example.Repository.LessonRepository;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/lessons")
public class LessonsController {

    private final LessonRepository repository;

    public LessonsController(LessonRepository repository) {
        this.repository = repository;
    }

    @GetMapping("")
    public Iterable<Lesson> all() {
        return this.repository.findAll();
    }

    @PostMapping("")
    public Lesson create(@RequestBody Lesson lesson) {
        return this.repository.save(lesson);
    }

    @PatchMapping("{id}")
    public Lesson patch(@PathVariable(value = "id") Long lessonId, @RequestBody Map requestParams) {
        return repository.findById(lessonId).map(lesson -> {
            if (requestParams.get("title") != null) {
                lesson.setTitle((String) requestParams.get("title"));
            }
            if (requestParams.get("deliveredOn") != null) {
                Calendar cal = Calendar.getInstance();
                String [] dateStringArray = ((String)requestParams.get("deliveredOn")).split("-");
                cal.set(Integer.parseInt(dateStringArray[0]), Integer.parseInt(dateStringArray[1]) - 1, Integer.parseInt(dateStringArray[2]));
                lesson.setDeliveredOn(cal.getTime());
            }
            return repository.save(lesson);
        }).orElseGet(() -> {
            return null;
        });
    }

    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Long lessonId)
            throws ResourceNotFoundException {
        Lesson lesson = repository.findById(lessonId)
                .orElseThrow(() -> new ResourceNotFoundException("Lesson not found for this id :: " + lessonId));

        repository.delete(lesson);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }


    @GetMapping("/find/{title}")
    public List<Lesson> findByTitle(@PathVariable(value = "title") String title)
            throws ResourceNotFoundException {
        return repository.findByLessonTitle(title);
    }

    @GetMapping("/between")
    public List<Lesson> findBetweenDates(@RequestParam(value = "date1") String date1, @RequestParam(value = "date2") String date2)
            throws ResourceNotFoundException {
        return repository.findByDeliveredOnDate(date1, date2);
    }
}
